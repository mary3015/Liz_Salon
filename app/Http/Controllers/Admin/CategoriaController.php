<?php

namespace multiventas\Http\Controllers\Admin;

use Illuminate\Http\Request;
use multiventas\Http\Controllers\Controller;
use multiventas\Models\Facturacion;
use multiventas\Models\Categoria;
use multiventas\Models\Servicio;
use multiventas\Models\Cliente;
use multiventas\Models\Producto;
use Illuminate\Support\Facades\Redirect;
use multiventas\Http\Requests\CategoriaFormRequest;
use DB;


class CategoriaController extends Controller
{
    public function __construct()
    {

    }
    public function index()
    {
        $categorias = Facturacion::all();
          $lista= Cliente::all() ;
          $servicios=Servicio::all();
          $producto=Producto::all();
          $numero_factura = Facturacion::all()->max();
          $categoria=Categoria::all();
          $suma=0;
          $listaProductos = Producto::all()->where('factura',$numero_factura->id);  

              return view('admin.categoria.index',["categorias"=>$categorias,"lista"=>$lista,"servicios"=>$servicios,
              "categoria"=>$categoria,"producto"=>$producto,"numero_factura"=>$numero_factura,"listaProductos"=>$listaProductos,"suma"=>$suma]);
        }
    
    public function store(Request $request)
    {
        $producto=new Producto;
        $producto->categoria=$request->get('servicios');
        $producto->servicio=$request->get('categorias');
        $producto->precio_unidad=$request->get('precio');
        $producto->cantidad=$request->get('cantidad');
        $producto->total_sin_iva=$request->get('resultado');
        $producto->iva=$request->get('iva');
        $producto->total=$request->get('totalIva');
        $producto->factura=$request->get('facturas')-1;
        $producto->save();
        return Redirect::to('admin/categoria');

    }
    public function numero_factura($id)
    {
                $categoria = Facturacion::all()->max();
              
                   
        }
        function mostrarservicios() {
           
         }
        
    public function edit($id)
    {
        return view("admin.categoria.edit",["categoria"=>Categoria::findOrFail($id)]);
    }
    public function update(CategoriaFormRequest $request,$id)
    {
        $categoria=Categoria::findOrFail($id);
        $categoria->nombre=$request->get('nombre');
        $categoria->descripcion=$request->get('descripcion');
        $categoria->update();
        return Redirect::to('admin/categoria');
    }
    public function destroy($id)
    {
        $categoria=categoria::findOrFail($id);
        $categoria->condicion='1';
        $categoria->update();
        return Redirect::to('admin/categoria');
    }
    
}

