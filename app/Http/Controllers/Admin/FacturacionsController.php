<?php

namespace multiventas\Http\Controllers\Admin;

use multiventas\Models\Facturacion;
use Illuminate\Http\Request;
use multiventas\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
class FacturacionsController extends Controller
{
      /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $facturacions = Facturacion::all();
       
       
        return view('admin.categoria.index',["facturacions"=>$facturacions]);
    }
   
    /**
     * almacena un nuevo recurso en la bd.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
   
        public function store(Request $request)
        {
            $facturacions=new Facturacion;
            $numero_factura = Facturacion::all()->max();
            $facturacions->id=$numero_factura->id+1;
            $facturacions->cliente=$request->get('nombres');
            $facturacions->total=$request->get('total');
            $facturacions->fecha=$request->get('fecha');
            $facturacions->save();
            return Redirect::to('admin/categoria');
    }

    
}
