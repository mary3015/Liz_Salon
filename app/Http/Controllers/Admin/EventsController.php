<?php

namespace multiventas\Http\Controllers\Admin;

use Illuminate\Http\Request;
use multiventas\Http\Controllers\Controller;
use Calendar;
use multiventas\Models\Event;

class EventsController extends Controller
{
    public function index()
    {
        $events = [];
        $data = Event::all();
        
        if($data->count()) {
            foreach ($data as $key => $value) {
                $events[] = Calendar::event(
                    $value->title,
                    false,
                   
                    new \DateTime($value->start_date),
                    new \DateTime($value->end_date),
    
                    null,
                    // Add color and link on event
	                [
	                    'color' => '#f05050',
	                    'url' => 'pass here url and any route',
	                ]
                );

            }
        }
        $calendar = Calendar::addEvents($events);
   return view('admin.Calendario.fullcalender', compact('calendar'));
    }
}
