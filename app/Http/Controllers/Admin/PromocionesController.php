<?php

namespace multiventas\Http\Controllers\Admin;

use Illuminate\Http\Request;
use multiventas\Http\Controllers\Controller;
use multiventas\Models\cliente;

class PromocionesController extends Controller
{
           /**
         * Send an e-mail reminder to the user.
         *
         * @param  Request  $request
         * @param  int  $id
         * @return Response
         */
        public function sendEmailReminder(Request $request)
        {
            $cliente = cliente::findOrFail($id);
            $cliente->email = $request->input('email');
    
            Mail::send('emails.reminder', [$cliente], function ($m) use ($cliente) {
                $m->from('hello@app.com', 'Your Application');
    
                $m->to($cliente->email, $cliente->name)->subject('Your Reminder!');
            });
    
}
}
