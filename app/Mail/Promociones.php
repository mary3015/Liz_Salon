<?php

namespace multiventas\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use multiventas\Models\Promocion;

class Promociones extends Mailable
{
    use Queueable, SerializesModels;

    public $promocionEnvio;

    public function __construct(Promocion $promocionEnvio)
    {
        $this->promocionEnvio = $promocionEnvio;
       
    }

    public function build()
    {
        return $this->view('emails.mailPromocion');
    }

}
