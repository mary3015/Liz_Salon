<?php

namespace multiventas\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Promocion extends Model
{
    use SoftDeletes;
    

    protected $table = 'promociones';
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    
    protected $primaryKey = 'id';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
         'descripcion',
         'descuento',
         'nombreProducto',
         'valido',
     ];
    
}