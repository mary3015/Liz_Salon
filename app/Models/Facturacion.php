<?php

namespace multiventas\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Facturacion extends Model
{
    protected $fillable = [
        'id',
        'cliente',
        'total',
    ];
}