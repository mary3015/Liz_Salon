<?php

namespace multiventas\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Producto extends Model
{
    use SoftDeletes;
    

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nombre',
         'categoria',
         'servicio',
         'precio_unidad',
         'cantidad',
         'total_sin_iva',
         'iva',
         'total',
         'factura',
               
     ];
    /**
     * obtener los productos de los clientes.
     */
    public function products()
    {
        return $this->hasMany('multiventas\Models\Producto','cliente_documento','documento');
    }
}