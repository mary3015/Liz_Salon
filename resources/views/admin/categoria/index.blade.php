@extends ('layouts.administracion_categoria')

@extends('layout')
@section ('contenido')
<!DOCTYPE html>
<html>
<head>
	
</head>
<body>
	<div class="row">
		
				<div class="container text-center"   >
				<div class="col-sm-3">
				<h4><dt>  Fecha: <?=date('m/d/Y');?></dt></h4>
			</div>
				<div class="col-sm-3">
					<h4><dt>Nit: xxxxxxx  </dt></h3>
					</div>
						<div class="col-sm-3">	
					<h4><dt>Empresa: Liz Salon  </dt> </dt></h3>	
				</div>
			</div>
			<div class="row">
					<div class="container text-center"   >
				<div class="col-sm-6">
					<label for="sel2">Documento Cliente:</label>
					 <select  id="select2" >
						 	<option>seleccione documento </option>
						  @foreach($lista as $cat)						  				
						    <option value="{{$cat->documento}}">{{$cat->documento}}</option>																
							@endforeach
							</select>
				</div>
				<div class="col-sm-6">
								<label  for="nombres">Nombre cliente :   </label>
								<label  id="nombre" name='nombres'></label>
								<label  id="apellido"></label>
				</div>
			</div>
		          <form method="post" action="{{ route('categoria.store') }}" data-parsley-validate ><class="form-horizontal form-label-left">
		          	{{ csrf_field() }}
			<div class="row">
					<div class="container text-center"   >
							<div class="col-sm-6">
									<label for="sel1">Categoria:</label>
							  <select  id="servicios" name="servicios">
									<option value=""> seleccione categoria</option>										  

									@foreach ($servicios as $cat)					
							<option value="{{$cat->id}}">
										 {{$cat->nombre}}</option>
											 @endforeach
									</select>
			</div>
							<label for="sel1">Servicios:</label>
								<select  id="categorias" name="categorias">									 					
							 	<option value=""> seleccione servicio </option>										  
									</select>
								</div>
					</div>
			</div>
			<div class="row">
			<div class="container text-center"   >

				<div class="col-sm-6">
					<label  for="nombres">Precio :   </label>
					<input  id="precio" name="precio" readonly="readonly"></input>
				</div>
				<label  for="nombres">cantidad :   </label>
				<input  id="cantidad" name="cantidad" ></input>
				<input  type="hidden" id "numero_factura" name="facturas" for="facturas" value="{{$numero_factura->id+1}}"  ></input>
				<button type="button" onclick="calcularResultado()">calcular Precio</button>
			</div>
	<div class="row">
		<div class="container text-center"   >
<label  for="nombrecantidad">cantidad total sin iva:   </label>
	<input  id="resultado" name="resultado"readonly="readonly"></input>
	<label  for="nombrecantidad2">Iva:   </label>
	<input  id="iva" name="iva" readonly="readonly"></input>
	<label  for="nombrecantidad3">cantidad total con iva:   </label>
	<input  id="totalIva" name="totalIva" readonly="readonly"></input>

</div>
</div>
<div class="container text-center"  > 

<button type="submit" class="btn btn-success">Agregar producto</button>
</div>
		</div>
			</div>
			</div>
			</div>
		</form>
<h3>Listado de Servicios </h3>
<div class="row">
		<div class="container text-center"   >
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<div class="table-responsive">
			<table class="table table-striped table-bordered table-condensed table-hover">
				<thead>
					<th>categoria</th>
					<th>servicio</th>
					<th>Precio Unidad</th>
					<th>Cantidad</th>
					<th>Total Sin Iva</th>
					<th>Iva</th>
					<th>Total</th>
					@if (count($listaProductos))
					@foreach ($listaProductos as $cat)
				</thead>
				<tr>
					<td>{{ $cat->categoria}}</td>
					<td>{{ $cat->servicio}}</td>
					<td>{{ $cat->precio_unidad}}</td>
					<td>{{ $cat->cantidad}}</td>
					<td>{{ $cat->total_sin_iva}}</td>
					<td>{{ $cat->iva}}</td>
					<td>{{ $cat->total}}</td>

				</tr>
				@endforeach
				@endif
				</tr>
							
			</table>
			<table class="table table-striped table-bordered table-condensed table-hover">
				<thead>
					@if (count($listaProductos))
					@foreach ($listaProductos as $cat)
			{{ $suma+=$cat->total}}
				</thead>
			    @endforeach
				@endif
					<th>Total</th>
					
			<th>{{ $suma}}</th>
				</tr>
							
			</table>
			<form method="post" action="{{ route('facturaciones.store') }}" data-parsley-validate ><class="form-horizontal form-label-left">
				{{ csrf_field() }}

				<button type="submit" class="btn btn-success">Nueva Factura</button>
		</div>
	</div>
</div>

</div>
<script type="text/javascript" src="{{asset('js/jQuery-2.1.4.min.js')}}"></script>
<script>
	$('#select2').change(function(){
		console.log($(this).val())
		$.get('/listaCategorias/'+$(this).val()).done(function(lista){
			$('#nombre').val(lista.nombre)
			console.log(lista.nombre)
			$('#apellido').val(lista.apellido)

		});
	});
	
	</script>
	<script type="text/javascript">
		$('#categorias').change(function(){
			console.log($(this).val())
			$.get('/listaprecio/'+$(this).val()).done(function(precio){
				$('#precio').val(precio.precio)
	
			});
		});
		
		</script>
		
		<script>
		function calcularResultado(){
			var cantidadProducto = $("#cantidad").val();
			var precio = $("#precio").val();
			var multiplicacion = cantidadProducto * precio;
			var iva=multiplicacion*0.19;
			var totalSinIva=multiplicacion-iva;
			
			$('#resultado').val(totalSinIva);
			$('#iva').val(iva);
			$('#totalIva').val(multiplicacion);
		}
	</script>
		
	<script type="text/javascript">
	$('#servicios').change(function(){
		console.log($(this).val())
		$.get('/listaCaServicios/'+$(this).val()).done(function(categorias){
			$('#categorias').html('')
			$('#categorias').append('<option value=""> seleccione servicio </option>')
			$.each(categorias,function(idnex,categoria){
				$('#categorias').append('<option value="'+categoria.id+'">'+categoria.nombre+'</option>');
			})
		})
	});
</script>

</body>
</html>
@endsection